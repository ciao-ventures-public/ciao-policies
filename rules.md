# Ciao Rules

We believe that thoughtful dialogue, sharing recommendations, and describing travel journies is the best way to travel and explore. In order to protect this experience and the safety of people who use Ciao, there are some limitations on the type of content and behavior that we allow. These limitations are set forth in the Ciao Rules below. Each participant in this community is responsible for maintaining these standards.

The Ciao Rules (along with all incorporated policies) [Privacy Policy](https://gitlab.com/ciao-ventures-public/ciao-policies/-/blob/main/privacy-policy.md) , and  [Terms of Service](https://gitlab.com/ciao-ventures-public/ciao-policies/-/blob/main/terms-of-service.md)   collectively make up the "Ciao User Agreement" that governs a user's access to and use of Ciao's services.

All individuals accessing or using Ciao's services must adhere to the policies set forth in the Ciao Rules. Failure to do so may result in Ciao taking one or more of the following enforcement actions:

*   requiring you to delete prohibited content before you can again create new posts and interact with other Ciao users;
*   temporarily limiting your ability to create posts or interact with other Ciao users;
*   asking you to verify account ownership with a phone number or email address; or
*   permanently suspending your account(s).

If you attempt to evade a permanent suspension by creating new accounts, we will suspend your new accounts.

We can change these Rules at any time. We keep a  [historical](https://gitlab.com/ciao-ventures-public/ciao-policies) record of all changes to our Rules publicly accessible on GitLab. By using Ciao on or after that effective date, you agree to the new Rules. If you don't agree to them, you should delete your account, otherwise your continued use of the site and content will be subject to the new Rules.

### Content Boundaries and Use of Ciao

#### Intellectual property

**Trademark:** We reserve the right to suspend accounts or take other appropriate action when someone's brand or trademark, including business name, author name, pseudonym, and/or logo, is used in a manner that may mislead or confuse others about your brand affiliation. Read more about our  [trademark policy and how to report a violation](https://gitlab.com/ciao-ventures-public/ciao-policies/-/blob/main/trademark-policy.md) .

**Copyright:** We will respond to clear and complete notices of alleged copyright infringement. Our copyright procedures are set forth in our [copyright and DMCA Policy](https://gitlab.com/ciao-ventures-public/ciao-policies/-/blob/main/copyright-and-dmca-policy.md).

#### Unlawful use

You may not use our service for any unlawful purposes or in furtherance of illegal activities. By using Ciao, you agree to comply with all applicable laws governing your online conduct and content.

#### Misuse of usernames

**Selling usernames:** You may not buy or sell Ciao usernames.

**Username squatting:** You may not engage in username squatting. Some of the factors we take into consideration when determining whether conduct is username squatting include:

*   the number of accounts created;
*   the creation of accounts for the purpose of preventing others from using those account names;
*   the creation of accounts for the purpose of selling those accounts; and
*   the use of third-party content feeds to update and maintain accounts under the names of those third parties.

Please note that Ciao may also remove accounts that are inactive for more than six months.

#### Abusive Behavior

We believe in freedom of expression and open dialogue. In order to ensure that people feel safe expressing diverse opinions and beliefs, we prohibit behavior that crosses the line into abuse, including behavior that harasses, intimidates, or uses fear to silence another user's voice. If you come across any abusive behavior please email us at [abuse@ciao.ventures](mailto:%abuse@ciao.ventures)  so we can take appropriate action.

#### Violence and physical harm

**Violence:** You may not make specific threats of violence or wish for the serious physical harm, death, or disease of an individual or group of people.

**Suicide or self-harm:** You may not promote or encourage suicide or self-harm.

**Child sexual exploitation:** You may not in anyway, directly or indirectly, promote child sexual exploitation. Learn more about our [zero-tolerance child sexual exploitation policy](https://gitlab.com/ciao-ventures-public/ciao-policies/-/blob/main/no-exploitation-policy.md) .

**Abuse and hateful conduct**

**Abuse:** You may not engage in the targeted harassment of someone, or incite other people to do so. We consider abusive behavior an attempt to harass, intimidate, or silence someone else's voice.

**Unwanted sexual advances:** You may not direct abuse at someone by sending unwanted sexual content, objectifying them in a sexually explicit manner, or otherwise engaging in sexual misconduct.

**Hateful conduct:** You may not promote violence against, threaten, or harass other people on the basis of race, ethnicity, national origin, sexual orientation, gender, gender identity, religious affiliation, age, disability, or serious disease.

**Hateful imagery and display names:** You may not use hateful images or symbols in your profile image or profile header. You also may not use your username, display name, or profile bio to engage in abusive behavior, such as targeted harassment or expressing hate towards a person, group, or protected category.   

**Private information and intimate media**

**Private information:** You may not publish or post other people's private information without their express authorization and permission. Definitions of private information may vary depending on local laws.

**Threats to expose / hack:** You may not threaten to expose someone's private information or intimate media. You also may not threaten to hack or break into someone's digital information.

**Impersonation**

You may not impersonate individuals, groups, or organizations in a manner that is intended to or does mislead, confuse, or deceive others. While you may maintain parody, fan, commentary, or newsfeed accounts, you may not do so if the intent of the account is to engage in spamming or abusive behavior.

#### Spam and Security

We strive to protect people on Ciao from technical abuse and spam.

To promote a stable and secure environment on Ciao, you may not do, or attempt to do, any of the following while accessing or using Ciao:

*   Access, tamper with, or use non-public areas of Ciao, Ciao's computer systems, or the technical delivery systems of Ciao's providers (except as expressly permitted by the Ciao Bug Bounty program).
*   Probe, scan, or test the vulnerability of any system or network, or breach or circumvent any security or authentication measures (except as expressly permitted by the Ciao Bug Bounty program).
*   Access or search, or attempt to access or search, Ciao by any means (automated or otherwise) other than through our currently available, published interfaces that are provided by Ciao (and only pursuant to the applicable terms and conditions), unless you have been specifically allowed to do so in a separate agreement with Ciao. Note that crawling Ciao is permissible if done in accordance with the provisions of the robots.txt file; however, scraping Ciao without our prior consent is expressly prohibited.
*   Forge any TCP/IP packet header or any part of the header information in any email or posting, or in any way use Ciao to send altered, deceptive, or false source-identifying information.
*   Interfere with or disrupt the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, mail-bombing Ciao's services, or by scripting the creation of content in such a manner as to interfere with or create an undue burden on Ciao.

Any accounts engaging in the following activities may be temporarily locked or subject to permanent suspension:

*   **Malware/Phishing:** You may not publish or link to malicious content intended to damage or disrupt another person's browser or computer or to compromise a person's privacy. 
*   **Spam:** You may not use Ciao's services for the purpose of spamming anyone. Spam is generally defined on Ciao as bulk or aggressive activity that attempts to manipulate or disrupt Ciao or the experience of users on Ciao to drive traffic or attention to unrelated accounts, products, services, or initiatives. Some of the factors that we take into account when determining what conduct is considered to be spamming include:
    *   if your posts consist mainly of links shared without commentary;
    *   if a large number of people have blocked you in response to high volumes of untargeted, unsolicited, or duplicative content or engagements from your account;
    *   if a large number of spam complaints have been filed against you;
    *   if you post duplicative or substantially similar content, replies, or mentions over multiple accounts or multiple duplicate updates on one account, or create duplicate or substantially similar accounts;
    *   if you send large numbers of unsolicited replies or mentions;
    *   if you add users to lists in a bulk or aggressive manner;
    *   if you are randomly or aggressively engaging with comments or users to drive traffic or attention to unrelated accounts, products, services, or initiatives;
    *   if you repeatedly post other people's account information as your own;
    *   if you post misleading, deceptive, or malicious links (e.g., affiliate links, links to malware/clickjacking pages, etc.);
    *   if you create fake accounts, account interactions, or impressions;
    *   if you sell, purchase, or attempt to artificially inflate account interactions; and

#### Content Visibility

Accounts under investigation or which have been detected as sharing content in violation of these Rules may have their account or comment visibility limited in Ciao.

#### Violations

Accounts under investigation or which have been detected as sharing content in violation of these Rules may have their account or comment visibility limited in Ciao.
